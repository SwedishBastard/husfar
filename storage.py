from starlette.config import Config
import databases
import enum
import sqlalchemy

config = Config('.env')
DATABASE_URL = config('DATABASE_URL')

class Unit(enum.Enum):
    none = 0
    gram = 1
    ml = 2
    tsp = 3
    tbsp = 4

metadata = sqlalchemy.MetaData()
ingredients = sqlalchemy.Table(
    'ingredients',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('name', sqlalchemy.String),
)
recipes = sqlalchemy.Table(
    'recipes',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('time_minutes', sqlalchemy.Integer),
    sqlalchemy.Column('name', sqlalchemy.String),
    sqlalchemy.Column('description', sqlalchemy.Text),
)
recipe_ingredients = sqlalchemy.Table(
    'recipe_ingredients',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('recipe_id', sqlalchemy.Integer),
    sqlalchemy.Column('ingredient_id', sqlalchemy.Integer),
    sqlalchemy.Column('quantity', sqlalchemy.Integer),
    sqlalchemy.Column('unit', sqlalchemy.Enum(Unit)),
)
recipe_instructions = sqlalchemy.Table(
    'recipe_instructions',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('recipe_id', sqlalchemy.Integer),
    sqlalchemy.Column('order', sqlalchemy.Integer),
    sqlalchemy.Column('description', sqlalchemy.Text),
)

database = databases.Database(DATABASE_URL)
