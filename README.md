# Web-based recipe collection software

This is a quick project for myself where I try out a bunch of Python software
that I haven't tried before (Starlette, SQLAlchemy, Alembic, Bootstrap) in a
somewhat controlled environment. The purpose of the project itself is to show
a bunch of recipes.

To install all required software, perform database migrations and start:

```shell
sudo pip3 install -r requirements.txt
alembic upgrade head
python3 app.py
```

## Current state and TODO list

Under *heavy* development, a project for my own relaxation.

* [ ]  Interface for recipes editing instead of doing SQL inserts by hand
* [ ]  Unit testing
* [ ]  Check if/how database calls can improve, because my current approach is
       somewhat naive and obviously done by someone just playing around
* [x]  Ingredient list in recipes
* [x]  Actual steps to take when preparing a recipe
* [ ]  Scaling ingredient list up and down, adding a default number of servings
       field to the recipe
* [ ]  Uploading of images
* [ ]  Personal menu available on login, saving selected recipes and the number
       of servings selected
* [ ]  Shopping list for personal menu
* [ ]  Actually spend some time on the interface, making it look somewhat
       attractive and non-developery
* [ ]  Support for i18n/l10n, adding Swedish as a first language
