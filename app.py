from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
import uvicorn

import storage

app = Starlette(debug=True)
app.mount('/static', StaticFiles(directory='static'), name='static')
templates = Jinja2Templates(directory='templates')

# - Prepare database when starting up ----------------------------------------
@app.on_event("startup")
async def startup():
    await storage.database.connect()


@app.on_event("shutdown")
async def shutdown():
    await storage.database.disconnect()


# - View functions -----------------------------------------------------------
@app.route('/')
async def homepage(request):
    query = storage.recipes.select()
    results = await storage.database.fetch_all(query)
    recipes = [
        {
            'id': result['id'],
            'name': result['name'],
            'description': result['description'],
            'time_minutes': result['time_minutes'],
        }
        for result in results
    ]
    context = {'request': request, 'recipes': recipes}
    return templates.TemplateResponse('index.html', context)


@app.route('/recipe/{id}')
async def recipe(request):
    # Let's see if we can find the recipe
    id = request.path_params['id']
    query = storage.recipes.select().where(storage.recipes.c.id == id)
    result = await storage.database.fetch_one(query)
    if result is None:
        return templates.TemplateResponse('404.html', {'request': request})
    recipe = {
        'name': result['name'],
        'description': result['description'],
        'time_minutes': result['time_minutes'],
    }

    # Get the instructions in order
    query = storage.recipe_instructions.select()
    query = query.where(storage.recipe_instructions.c.recipe_id == id)
    query = query.order_by(storage.recipe_instructions.c.order)
    results = await storage.database.fetch_all(query)
    recipe['instructions'] = [result['description'] for result in results]

    # All ingredients now
    query = storage.recipe_ingredients.select()
    query = query.where(storage.recipe_ingredients.c.recipe_id == id)
    results = await storage.database.fetch_all(query)
    recipe['ingredients'] = [
        {
            'ingredient_id': result['ingredient_id'],
            'quantity': result['quantity'],
            'unit': result['unit'].name,
        }
        for result in results
    ]

    # We still don't have the ingredient names yet, just the database ID:s for
    # them. That's about to change now.
    for ingredient in recipe['ingredients']:
        id = ingredient['ingredient_id']
        query = storage.ingredients.select()
        query = query.where(storage.ingredients.c.id == id)
        result = await storage.database.fetch_one(query)
        ingredient['name'] = result['name']

    # Ready to render
    context = {'request': request, 'recipe': recipe}
    return templates.TemplateResponse('recipe.html', context)

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8000)
