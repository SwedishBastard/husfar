"""Create recipe ingredients table"""

from alembic import op
import sqlalchemy as sa
import storage

# Revision identifiers, used by Alembic
revision = 'f1c4d144cd62'
down_revision = 'bc2f196427c8'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'ingredients',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(64), unique=True, nullable=False),
    )
    op.create_table(
        'recipe_ingredients',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('recipe_id', sa.Integer, nullable=False),
        sa.Column('ingredient_id', sa.Integer, nullable=False),
        sa.Column('quantity', sa.Integer, nullable=False),
        sa.Column('unit', sa.Enum(storage.Unit), nullable=False),
    )


def downgrade():
    op.drop_table('ingredients')
    op.drop_table('recipe_ingredients')
