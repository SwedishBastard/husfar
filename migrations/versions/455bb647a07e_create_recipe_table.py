"""Create recipe table"""
from alembic import op
import sqlalchemy as sa

# Revision identifiers, used by Alembic
revision = '455bb647a07e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'recipes',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('time_minutes', sa.Integer, nullable=False),
        sa.Column('name', sa.String(64), nullable=False),
        sa.Column('description', sa.Text, nullable=False),
    )


def downgrade():
    op.drop_table('recipes')
