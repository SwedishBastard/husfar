"""Create recipe instructions table"""

from alembic import op
import sqlalchemy as sa

# Revision identifiers, used by Alembic
revision = 'bc2f196427c8'
down_revision = '455bb647a07e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'recipe_instructions',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('recipe_id', sa.Integer, nullable=False),
        sa.Column('order', sa.Integer, nullable=False),
        sa.Column('description', sa.Text, nullable=False),
    )


def downgrade():
    op.drop_table('recipe_instructions')
